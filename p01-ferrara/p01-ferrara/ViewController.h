//
//  ViewController.h
//  p01-ferrara
//
//  Created by Dylan Ferrara on 1/18/17.
//  Copyright © 2017 Dylan Ferrara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, strong) IBOutlet UILabel *helloLabel;
@property (nonatomic, strong) IBOutlet UITextField *inputField;

- (IBAction)clicked:(id)sender;

@end

