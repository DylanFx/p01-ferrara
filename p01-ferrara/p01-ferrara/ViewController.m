//
//  ViewController.m
//  p01-ferrara
//
//  Created by Dylan Ferrara on 1/18/17.
//  Copyright © 2017 Dylan Ferrara. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize helloLabel;
@synthesize inputField;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clicked:(id)sender {
    //[helloLabel setText:@"I am your phone"];
    
    NSString *yourName = self.inputField.text;
    NSString *message;
    
    if ([yourName length] == 0) {
        yourName = @", again, World";
    }
    
    message = [NSString stringWithFormat:@"Hello, %@ . I am your phone", yourName];
    
    self.helloLabel.text = message;
}

@end
