//
//  Button.m
//  p01-ferrara
//
//  Created by Dylan Ferrara on 1/19/17.
//  Copyright © 2017 Dylan Ferrara. All rights reserved.
//

#import "Button.h"

@implementation Button

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    NSLog(@"Touched Button");
}

@end
